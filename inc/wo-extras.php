<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * This is functionality added by Aurooba Ahmed
 *
 * @package Copperstone_Theme
 */


/**
 * Remove paragraph tags around images
 */
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('get_archive_description', 'filter_ptags_on_images');

/**
 * Remove paragraph tags in blockquotes
 */
function filter_ptags_on_blockquotes($content){
    $find = array('<blockquote class="ts-block"><p>', '</p></blockquote>');
    $replace = array('<blockquote class="ts-block">','</blockquote>');
    return str_replace($find, $replace, $content);
}
add_filter('the_content', 'filter_ptags_on_blockquotes');

/**
 * Return ACF sub field without wpautop
 */
function the_sub_field_without_wpautop( $field_name ) {
	
	remove_filter('acf_the_content', 'wpautop');
	
	echo the_sub_field( $field_name );
    
	add_filter('acf_the_content', 'wpautop');
}

/**
 * Deregister WordPress-included jQuery and include Google's
 */
if (!is_admin()) add_action("wp_enqueue_scripts", "wanderoak_jquery_enqueue", 11);
function wanderoak_jquery_enqueue() {
//    wp_deregister_script('jquery');
    
//    wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", '', null, false);
    
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'ad-fontawesome-loader', get_template_directory_uri() . '/js/fontawesome.min.js#asyncload', 'jquery', '', true );
//    wp_enqueue_script( 'ad-fontawesome-light', get_template_directory_uri() . '/js/light.min.js#asyncload', 'jquery', '', true );
    wp_enqueue_script( 'ad-fontawesome-regular', get_template_directory_uri() . '/js/regular.min.js#asyncload', 'jquery', '', true );
//    wp_enqueue_script( 'ad-fontawesome-solid', get_template_directory_uri() . '/js/solid.min.js#asyncload', 'jquery', '', true );
//    wp_enqueue_script( 'ad-fontawesome-brands', get_template_directory_uri() . '/js/brands.min.js#asyncload', 'jquery', '', true );
}



/**
 * Returns a cleaner Navigation
 */

function cleanernav($location) {
    $clearnav = wp_nav_menu(array(
                    'theme_location' => $location,
                    'container' => false,
                    'items_wrap' => '%3$s', 
                    'echo' => false,
                    ));

    $find = array('><a', '<li');
    $replace = array('','<a');
    $newnav = str_replace($find, $replace, $clearnav);
    echo $newnav;
}

/**
 * ACF Theme Options Page
 */
//if( function_exists('acf_add_options_page') ) {
//	
//	acf_add_options_page();
//	acf_set_options_page_title( __('Theme Options') );
//}
//if( function_exists('acf_add_options_page') ) {
//	
//	acf_add_options_page(array(
//		'page_title' 	=> 'Theme General Settings',
//		'menu_title'	=> 'Theme Options',
//		'menu_slug' 	=> 'theme-general-settings',
//		'capability'	=> 'edit_posts',
//		'redirect'		=> false,
//        'parent_slug'	=> 'themes.php'
//	));
//	
//}
/**
 * Numbered pagination
 */
function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}


/**
* Remove prefix on archive pages
*/

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

/**
* Include editor style stylesheet.
*/
add_theme_support('editor-styles');
add_editor_style('editor-style.css');

/**
* Hide WordPress Update Nag to All But Admins
*/
function hide_update_notice_to_all_but_admin() {
    if ( !current_user_can( 'update_core' ) ) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin', 1 );

/**
* Disable the Emoji Nonsense
*/
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

/**
* Media should also have categories, don't you agree?
*/

function add_categories_for_attachments() {
    register_taxonomy_for_object_type( 'category', 'attachment' );
}
add_action( 'init' , 'add_categories_for_attachments' );


/**
* No one needs the xmlrpc vulnerability.
*/
add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
//include_once('acf-gp.php');

/**
 * ACF Theme Options Page
 */
//if( function_exists('acf_add_options_page') ) {
//	
//	acf_add_options_page();
//	acf_set_options_page_title( __('Theme Options') );
//}

/**
 * Callback function to insert 'styleselect' into the top toolbar in TinyMCE
 */
function my_mce_buttons( $buttons ) {
    
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons', 'my_mce_buttons' );


// Register our callback to the appropriate filter
//add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Subtitle',  
			'inline' => 'span',  
			'classes' => 'subtitle-format',
            'wrapper' => true,
		),
        array(  
			'title' => 'Highlighted Statement',  
			'inline' => 'span',  
			'classes' => 'highlight-statement',
            'wrapper' => true,
		),
//		array(  
//			'title' => '.ltr⇢',  
//			'block' => 'blockquote',  
//			'classes' => 'ltr',
//			'wrapper' => true,
//		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 

// define the enqueue_embed_scripts callback 
function action_enqueue_embed_scripts(  ) { 
    wp_enqueue_style('embed-style', get_stylesheet_directory_uri() . '/embed-style.css');
}; 
         
// add the action 
add_action( 'enqueue_embed_scripts', 'action_enqueue_embed_scripts', 10, 0 ); 