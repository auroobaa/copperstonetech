<?php
/**
 * WordPress backend modifications by Wanderoak
 *
 * This is functionality added by Aurooba Ahmed
 *
 * @package Copperstone_Theme
 */


//add_filter('acf/settings/show_admin', '__return_false');

function custom_menu_page_removing() {
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'edit-comments.php' );
//    remove_menu_page( 'edit.php' );
//    remove_menu_page( 'tools.php' ); 
}
add_action( 'admin_menu', 'custom_menu_page_removing' );


/**
 * Add Visual Blocks external plugin to TinyMCE
 */

function add_mceplugins() {
     $plugin = array('visualblocks');
     $plugins_array = array();

     foreach ($plugin as $plugin ) {
          $plugins_array[ $plugin ] = get_stylesheet_directory_uri() . '/js/'. $plugin .'/plugin.js';
     }
     return $plugins_array;
}
add_filter('mce_external_plugins', 'add_mceplugins');

function register_visualblocks($in) {
    $in['visualblocks_default_state'] = 'true';
    return $in;
}
add_filter('tiny_mce_before_init', 'register_visualblocks'); 

/**
 * Add Wanderoak credit to footer in backend
 */

function remove_footer_admin () {

echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Designed & Developed by <a href="http://wanderoak.co" target="_blank">Wanderoak</a>';

}

add_filter('admin_footer_text', 'remove_footer_admin');

/**
* Add Support Widget
**/ 

function support_add_dashboard_widgets() {

  wp_add_dashboard_widget('wp_dashboard_widget', 'Support', 'support_info');
    
// Globalize the metaboxes array, this holds all the widgets for wp-admin
 
 	global $wp_meta_boxes;
 	
 	// Get the regular dashboard widgets array 
 	// (which has our new widget already but at the end)
 
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	
 	// Backup and delete our new dashboard widget from the end of the array
 
 	$example_widget_backup = array( 'wp_dashboard_widget' => $normal_dashboard['wp_dashboard_widget'] );
 	unset( $normal_dashboard['wp_dashboard_widget'] );
 
 	// Merge the two arrays together so our widget is at the beginning
 
 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 
 	// Save the sorted array back into the original metaboxes 
 
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;

}

add_action('wp_dashboard_setup', 'support_add_dashboard_widgets' );

function support_info() { ?>

<div class="custom-welcome-panel-content">
	<h3 style="padding-left: 0; font-size: 16px;">Welcome to Your Dashboard!</h3>
	<p><?php _e( 'This dashboard is powered by Wordpress and customized to fit your needs by Wanderoak' ); ?></p>
	<div class="welcome-panel-column-container" style="overflow: hidden;">
	<div class="welcome-panel-column" style="float: left; width: 40%; margin-right: 2em;">
		<h4><?php _e( 'Next Steps' ); ?></h4>
		<ul>
		<?php if ( 'page' == get_option( 'show_on_front' ) && ! get_option( 'page_for_posts' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-edit-page">' . __( 'Edit your front page' ) . '</a>', get_edit_post_link( get_option( 'page_on_front' ) ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
		<?php elseif ( 'page' == get_option( 'show_on_front' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Create an article' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php else : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Write your first article' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php endif; ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-view-site">' . __( 'View your site' ) . '</a>', home_url( '/' ) ); ?></li>
		</ul>
			<div>
		<h4><?php _e( "Questions?" ); ?></h4>
		<a class="button button-primary button-hero load-customize hide-if-no-customize" href="mailto:hey@wanderoak.co"><?php _e( 'Email Us!' ); ?></a>
<!--			<p class="hide-if-no-customize"><?php printf( __( 'or, <a href="%s">edit your site settings</a>' ), admin_url( 'options-general.php' ) ); ?></p>-->
	</div>
	</div>
	<div class="welcome-panel-column welcome-panel-last">
		<h4><?php _e( 'More Actions' ); ?></h4>
		<ul>
			<li><?php printf( '<div class="welcome-icon welcome-widgets-menus">' . __( 'Manage your <a href="%1$s">navigation menu</a>' ) . '</div>', admin_url( 'nav-menus.php' ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-comments">' . __( 'Turn comments on or off' ) . '</a>', admin_url( 'options-discussion.php' ) ); ?></li>
			<!--<li><php printf( '<a href="%s" class="welcome-icon welcome-learn-more">' . __( 'Learn more about getting started' ) . '</a>', __( 'http://localhost/polishedarrow/wp-admin/index.php?page=wo-walkthrough' ) ); ?></li>-->
		</ul>
	</div>
       <br />
        <h4><?php _e( "Confused?" ); ?></h4>
            <a class="button button-primary button-hero load-customize hide-if-no-customize" href="https://cseb-scbe.org/wpmarine/wp-admin/index.php?page=wo-walkthrough"><?php _e( 'Watch the tutorial!' ); ?></a>
    <!--			<p class="hide-if-no-customize"><?php printf( __( 'or, <a href="%s">edit your site settings</a>' ), admin_url( 'options-general.php' ) ); ?></p>-->
	</div>
</div>
 
  
<?php }

include_once('acf-smart-button/acf-smart-button.php');

