<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Copperstone_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    if (get_the_post_thumbnail()): 
        the_post_thumbnail();
    endif; 
    ?>
	<header class="entry-header">
		<div class="entry-meta">
			<?php wocopperstone_posted_on(); ?>
		</div><!-- .entry-meta -->
        <?php 
        the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
            if (!get_field('excerpt_or_full')) :
                $excerpt = get_the_excerpt();
                $result = substr($excerpt, 0, strrpos($excerpt, '.'));
                echo $result . '.';
            else :
                the_content();
            endif;
		?>
		 
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<a class="read-more-link" href="<?php echo get_permalink(); ?>">Read</a>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
