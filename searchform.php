<?php
/**
 * Template for the search form!
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Tempest
 */

?>
   

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) )?>">
    <label>
        <span class="screen-reader-text">Search for:..</span>
        <i class="fa-search"></i>
        <input type="search" class="search-field" placeholder="" value="" name="s" title="" />
    </label>
    <input type="submit" class="search-submit" value="search" />
</form>