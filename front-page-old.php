<?php
/**
 * This is the front page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Copperstone_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="page-builder">
            <?php

            // check if the flexible content field has rows of data
            if( have_rows('content_matrix') ):

                 // loop through the rows of data
                while ( have_rows('content_matrix') ) : the_row();

                    if( get_row_layout() == 'cta_duo' ):

                        get_template_part( 'builder/builder', 'cta' );

                    elseif( get_row_layout() == 'details_block' ): 

                        get_template_part( 'builder/builder', 'details' );

                    elseif( get_row_layout() == 'testimonial' ): 

                        get_template_part( 'builder/builder', 'testimonial' );

                    elseif( get_row_layout() == 'simple_text_editor' ): 

                        get_template_part( 'builder/builder', 'editor' );
            
                    elseif( get_row_layout() == 'column_content' ): 

                        get_template_part( 'builder/builder', 'product' );

                    endif;

                endwhile;

            else :

                // no layouts found

            endif;

            ?>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
