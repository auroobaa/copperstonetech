<?php

$ctas = get_sub_field('call_to_actions');



?>
<section class="page-builder-section cta-duo">
    <?php if( have_rows('call_to_actions') ): ?>
        <?php while( have_rows('call_to_actions') ): the_row(); 

            // vars
            $bgimg = get_sub_field('image');
            $heading = get_sub_field('heading');
            $content = get_sub_field('description');
            $button = get_sub_field('button');
            ?>

               <div class="cta">
                    <div class="heading" style="background-image: url('<?php echo $bgimg; ?>')">
                            <h3><?php echo $heading; ?></h3>
                    </div>
                    <div class="cta-content">
                        <?php echo $content; ?>
                        <div><a class="learn-more" href="<?php echo $button['url']; ?>"><?php echo $button['text']; ?> </a></div>
                    </div>
                </div>

            
        <?php endwhile; ?>

        

    <?php endif; ?>
</section>


