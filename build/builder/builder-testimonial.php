<section class="page-builder-section testimonial">
    <div class="testimonial"><span class="big-quote"><img src="<?php echo get_template_directory_uri()?>/imgs/Testimonial_quotes.svg"></span>
      <span class="testimonial-text">
      <?php the_sub_field('testimonial_text');?>
      <p class="testimonial-author"><?php the_sub_field('testimonial_author'); ?></p>
      </span>
    </div>
</section>