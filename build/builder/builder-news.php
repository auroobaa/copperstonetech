<?php

/**
 * Latest News Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'latest-news-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'latest-news padding-left ';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$post_type = get_field('post_selection');
$posts = array();
if ($post_type != 'recent'): 
    $posts = get_field('choose_posts');
    array_splice($posts, 3);
else :
    $posts_array = wp_get_recent_posts(array(
        'numberposts' => 3, // Number of recent posts 
        'post_status' => 'publish' // Show only the published posts
    ));
    foreach ($posts_array as $post) :
        $posts[] = $post['ID'];
    endforeach;
endif;

?>
<section id="<?php echo esc_attr($id); ?>" class="builder <?php echo esc_attr($className); ?>">
    <div class="content-wrapper">
        <h3>Latest News</h3>
        <ul class="article-list">
        <?php foreach ($posts as $post) : ?>
            <li><article>
                <h5><?php echo get_the_title($post); ?></h5>
                <div class="excerpt">
                    <?php 
                    $excerpt = get_the_excerpt($post);
                    $result = substr($excerpt, 0, strrpos($excerpt, '.'));
                    echo $result . '.';
                    ?>
                </div>
                <a class="read-more-link" href="<?php echo get_permalink($post); ?>">Read</a>
            </article></li>
        <?php endforeach; ?>
        </ul>
    </div>
</section>