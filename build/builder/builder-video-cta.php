<?php

/**
 * Video CTA Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'video-cta-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'video-cta';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$video = get_field('video');
$description = get_field('description');
$button = get_field('button');
//echo '<pre>' . var_export($button, true) . '</pre>';
?>

<section id="<?php echo esc_attr($id); ?>" class="builder <?php echo esc_attr($className); ?>">
    <div class="content-wrapper">
        <video autoplay muted loop playsinline id="bg-video">
          <source src="<?php echo $video; ?>" type="video/mp4">
        </video>

        <div class="content">
            <h2><span><?php echo $description; ?></span></h2>
            <?php if ($button): ?>
            <div class="button-wrapper">
                <a class="cst-button" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>


<!--
<div id="videoDiv"> 
    <div id="videoBlock">   
        <video preload="preload"  id="video" autoplay="autoplay" loop="loop">
            <source src="<?php echo $video; ?>" type="video/mp4"></source>
        </video> 
        <div id="videoMessage">
            <h1 class="spacer">The Early Bird...</h1>
            <h2 class="spacer">...has the best holiday.</h2>
            <h3 class="spacer">Boat rentals in France</h3>
            <p class="videoClick" >
                <a href="https://www.hotelsafloat.com/home-away.php">Click here and be impressed</a>
            </p> 
        </div>
    </div>
</div>-->
