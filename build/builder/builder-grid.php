
<?php

/**
 * Video CTA Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'grid-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'grid';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
$tall = get_field('tall_image') ? 'tall-images' : '';
// Load values and assing defaults.
$grid_count = count(get_field('grid_items'));

$grid_class = ($grid_count >= 3) ? 'lots' : $grid_count;

?>
<section id="<?php echo esc_attr($id); ?>" class="builder <?php echo esc_attr($className); echo ' ' . $tall; ?> <?php echo 'grid-count-' . $grid_class; ?> ">

    <?php if( have_rows('grid_items') ): ?>

        <?php while( have_rows('grid_items') ): the_row(); 

            // vars
            $bgimg = get_sub_field('background_image');
            $heading = get_sub_field('heading');
            $content = get_sub_field('description');
            $button = get_sub_field('button');
            ?>

               <div class="grid-item <?php if ($button) : echo 'has-button'; endif; ?>">
                    <div class="heading" style="background-image: url('<?php echo $bgimg; ?>')">
                            <h3><?php echo $heading; ?></h3>
                    </div>
                    <div class="grid-item-content">
                        <?php echo $content; ?>
                        <?php if ($button) : ?>
                        <div><a class="learn-more" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?> </a></div>
                        <?php endif; ?>
                    </div>
                </div>

            
        <?php endwhile; ?>

        

    <?php endif; ?>
    
</section>


