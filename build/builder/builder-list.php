<?php

/**
 * Detailed List Block Template
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'detailed-list-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'detailed-list padding-left ';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$style = get_field('style');
$heading = get_field('heading');
$description = get_field('description');
$list = get_field('list');

?>
<section id="<?php echo esc_attr($id); ?>" class="builder <?php echo esc_attr($className) . ' ' . $style; ?>">
    <?php if ($heading) : ?>
        <h3><?php echo $heading; ?></h3>
    <?php endif; ?>
    <div class="content-wrapper">
        <div class="description">
            <?php echo $description; ?>
        </div>
        
        <ul class="list-wrapper">
        <?php foreach ($list as $item) : ?>
            <li>
                <h4><?php echo $item['heading']; ?></h4>
                <div class="content-wrapper">
                    <div class="content">
                        <?php echo $item['content']; ?>
                    </div>
                    <?php if ($style == 'imgs') : ?>
                    <figure>
                        <img src="<?php echo $item['image']['url']; ?>">
                    </figure>
                    <?php endif; ?>
                </div>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>
</section>