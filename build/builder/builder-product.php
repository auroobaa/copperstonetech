<?php

$img = get_sub_field('image');
$text = get_sub_field('text');

?>
    
<section class="page-builder-section product-highlight">

    <div class="product-text">
       <?php echo $text; ?> 
    </div>
    <div class="product-image">
        <img src="<?php echo $img; ?>" />
    </div>
</section>