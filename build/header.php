<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Copperstone_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'groundup' ); ?></a>

    <?php if ( is_front_page()) : ?>
        <header id="masthead" class="site-header front-page-header" role="banner">
    <?php else: ?>
	    <header id="masthead" class="site-header" role="banner">
    <?php endif; ?>
       
        <div class="site-branding">
            <?php if (is_front_page()) : ?>
                <h1 class="site-title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri()?>/imgs/190506_Copperstone_Logo_Black.png" alt="<?php bloginfo( 'name' ); ?>"></a>
                </h1>
            <?php else: ?>
                <h2 class="site-title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri()?>/imgs/190506_Copperstone_Logo_Black.png" alt="<?php bloginfo( 'name' ); ?>"></a>
                </h2>
            <?php endif;?>
<!--
			 <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="cst-mark-wrapper"  rel="home">
			     <img class="cst-mark sticky-mark" src="<?php echo get_template_directory_uri()?>/imgs/copperstone_mark_black.png">
			 </a>
-->
		</div><!-- .site-branding -->
		<?php if (get_field('display') || is_single() || is_home() || is_404() || is_search() || is_archive() ): ?>
            <?php if (is_single() || is_home() || is_archive()) : ?>
                <div class="page-title page-for-posts">
                    <h1><a href="<?php echo get_permalink(get_option( 'page_for_posts' )); ?>"><?php echo get_the_title(get_option( 'page_for_posts' )); ?></a></h1>
                </div>
            <?php elseif (is_search()) : ?>
                <div class="page-title">
                    <h1>Search</h1>
                </div>         
            
            <?php elseif (is_404()) : ?>
                <div class="page-title">
                    <h1><?php wp_title(''); ?></h1>
                </div>
            
            <?php else : ?>
                <div class="page-title">
                    <h1><?php the_title(); ?></h1>
                </div>
            <?php endif; ?>
		<?php endif; ?>
	</header><!-- #masthead -->
    <nav id="site-navigation" class="main-navigation" role="navigation">
        <div class="mobile-nav">
            <span class="nav-open" onclick="openNav()"><i class="far fa-ellipsis-h fa-2x"></i> Menu</span>
            <span class="nav-close" onclick="closeNav()"><i class="far fa-times "></i> Menu</span>
        </div>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="cst-mark-wrapper cst-mark-mobile-wrapper"  rel="home">
             <img class="cst-mark mobile-cst-mark" src="<?php echo get_template_directory_uri()?>/imgs/copperstone_mark.png">
        </a>
        <div id="menu-wrapper">
            <?php 
                $nav_locations = get_nav_menu_locations();
                $current_menu = wp_get_nav_menu_object($nav_locations['primary']);
                $nav_cta = get_field('call_to_action', $current_menu);
                $site_description = get_bloginfo( 'description' );
                echo '<div class="wrapper">';
                echo '<div class="nav-meta-wrapper"><p class="nav-desc">' . $site_description . '</p><a class="nav-cta" href="' . $nav_cta['url'] . '"><span>' . $nav_cta['title'] . '</span></a></div>';
                echo '<div class="nav-search"><a href="' . esc_url( home_url( '/' ) ) . '/search"><i class="far fa-search"></i> <span>Search</span></a></div>';
                echo '</div>';
                wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'div', 'container_id' => 'main-menu', 'menu_id' => 'primary', 'walker' => new CST_Walker() ) ); 
            ?>
        </div>
    </nav><!-- #site-navigation -->

	<div id="content" class="site-content">
