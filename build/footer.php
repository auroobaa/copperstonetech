<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Copperstone_Theme
 */

?>

	</div>  
	<?php if (get_field('image', $pid) ) : ?>
        <div class="footer-image">
            <img src="<?php the_field('image', $pid); ?>" />
        </div>
    <?php endif; ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="light-footer">
            
        </div>
        <div class="transparent-footer">
            
        </div>
        <div class="menu-footer">
            <?php                 
            wp_nav_menu( array( 'theme_location' => 'footer', 'container' => 'div', 'container_id' => 'footer-menu', 'menu_id' => 'footer' ) ); 
            ?>
        </div>
        <div class="dgreen-footer">
            
        </div>
        <div class="site-info-footer">
            <div class="site-info">
                <span class="site-copyright">&copy; <?php echo date("Y"); ?> <?php echo get_bloginfo('name'); ?> Ltd. All rights reserved.</span>
            </div><!-- .site-info -->
            <section class="bottom-hero">
                <a class="bottom-hero-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img src="<?php echo get_template_directory_uri()?>/imgs/190508_Copperstone_Logo_White.png">
                </a>
            </section>
        </div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
    
    function openNav() {
        var element = document.getElementById("site-navigation");
        element.classList.add("overlay-nav");
//        document.getElementById("site-navigation").style.width = "13vw";
//        document.getElementById("closebtn").style.display = "block";
    }

    function closeNav() {
        var element = document.getElementById("site-navigation");
        element.classList.remove("overlay-nav");
//        document.getElementById("menu-wrapper").style.right = "-100%";
//        document.getElementById("closebtn").style.display = "none";
    }
</script>
<!--
<script>
jQuery('p').filter(function() {
    var $childNodes = jQuery(this).contents();
//    console.log($childNodes);
    return $childNodes.not($childNodes.filter('a').first()).not(function() {
        return this.nodeType === 3 && jQuery.trim(this.nodeValue) === '';
    }).length === 0;
}).addClass('lonesome-link');
</script>
-->
<script>

//jQuery('.cst-mark img').scroll(function(){}).addClass('sticky-mark');
</script>

</body>
</html>
