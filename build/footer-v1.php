<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Copperstone_Theme
 */

?>

	</div><!-- #content -->
	<?php if (get_field('image', $pid) ) : ?>
        <div class="footer-image">
            <img src="<?php the_field('image', $pid); ?>" />
        </div>
    <?php endif; ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
      <section class="bottom-hero">
        <a class="bottom-hero-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img src="<?php echo get_template_directory_uri()?>/imgs/190508_Copperstone_Logo_White.png">
        </a>
      </section>
		<div class="site-info">
		    <span class="site-copyright">&copy; <?php echo date("Y"); ?> <?php echo get_bloginfo('name'); ?> Ltd. All rights reserved.</span>
			<span class="site-credit"><?php printf( 'Built with <img style="width: 16px;" class ="footer-heart" src="' . get_template_directory_uri() . '/imgs/fa-heart.png"> by %2$s.', 'Wanderoak', '<a href="http://wanderoak.co/" rel="designer">Wanderoak</a>' ); ?></span>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<!--
<script>
jQuery('p').filter(function() {
    var $childNodes = jQuery(this).contents();
//    console.log($childNodes);
    return $childNodes.not($childNodes.filter('a').first()).not(function() {
        return this.nodeType === 3 && jQuery.trim(this.nodeValue) === '';
    }).length === 0;
}).addClass('lonesome-link');
</script>
-->
<script>

//jQuery('.cst-mark img').scroll(function(){}).addClass('sticky-mark');
</script>

</body>
</html>
