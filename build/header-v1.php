<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Copperstone_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'groundup' ); ?></a>

    <?php if ( is_front_page()) : ?>
        <header id="masthead" class="site-header front-page-header" role="banner">
            <div class="site-branding">
    <?php else: ?>
	    <header id="masthead" class="site-header" role="banner">
	        <div class="site-branding">
    <?php endif; ?>
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri()?>/imgs/190508_Copperstone_Logo_White.png" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
            <?php elseif ( is_front_page() && ! is_home()) : ?>
                <div class="site-hero">
    				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri()?>/imgs/190508_Copperstone_Logo_White.png" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
                </div>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri()?>/imgs/190508_Copperstone_Logo_White.png" alt="<?php bloginfo( 'name' ); ?>"></a></p>
			<?php
			endif;?>
		</div><!-- .site-branding -->
        <span class="mob-nav">Menu</span>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php cleanernav('primary');
            //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); 
            ?>
		</nav><!-- #site-navigation -->
        <div id="mobile-site-navigation-wrapper">
            <nav id="mobile-site-navigation" class="main-navigation" role="navigation">
                <?php cleanernav('primary');
                //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); 
                ?>
            </nav><!-- #site-navigation -->
		</div>
		<script>
            var PI = { 
                onReady: function() {
                    jQuery('.mob-nav').click( PI.mobnav );  
                },

                mobnav: function(event) {
                    jQuery('#mobile-site-navigation-wrapper').slideToggle(500);
                },
                
                bignav: function() {
                    if (jQuery(window).width() >= 914) {
                        jQuery('#mobile-site-navigation-wrapper').hide();
                    }
                }

            };

            jQuery(document).ready(PI.onReady);
            jQuery(document).ready(PI.parentlink);
            jQuery(window).resize(PI.bignav);
        </script>
	</header><!-- #masthead -->
    <div class="featured-image">
    <?php
        $pid = get_the_ID();
        $fimg = get_the_post_thumbnail($pid);  
        echo $fimg;
    ?>
<!--            <img class="header-image" src="<?php the_field('header_image'); ?>" />-->

    </div>
	<div id="content" class="site-content">
