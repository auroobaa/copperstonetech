<?php

add_filter( 'allowed_block_types', 'wo_allowed_block_types' );
 
function wo_allowed_block_types( $allowed_blocks ) {
 
	return array(
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/list',
        'core/pullquote',
        'core/gallery',
        'core/columns',
        'core/embed',
        'core/video',
        'core/audio',
        'core/shortcode',
        'core/embed',
        'gravityforms/form',
        'core-embed/twitter',
        'core-embed/youtube',
        'core-embed/facebook',
        'core-embed/instagram',
        'core-embed/wordpress',
        'core-embed/soundcloud',
        'core-embed/spotify',
        'core-embed/flickr',
        'core-embed/vimeo',
        'core-embed/kickstarter',
        'core-embed/meetup-com',
        'core-embed/polldaddy',
        'core-embed/reddit',
        'core-embed/screencast',
        'core-embed/scribd',
        'core-embed/slideshare',
        'core-embed/speaker',
        'core-embed/ted',
        'core-embed/tumblr',
        'core-embed/videopress',
        'core-embed/wordpress-tv',
        'acf/latest-news',
        'acf/video-cta',
        'acf/detailed-list',
        'acf/tech-specs',   
        'acf/highlight',
        'acf/grid',
	);
 
}

function wo_register_acf_block_types() {

    // register a latest-news block.
    acf_register_block_type(array(
        'name'              => 'latest-news',
        'title'             => __('Latest News'),
        'description'       => __('Display the latest 3 posts.'),
        'render_template'   => 'builder/builder-news.php',
        'category'          => 'layout',
        'icon'              => 'book',
        'keywords'          => array( 'latest', 'news', 'cst' ),
        'supports' => array( 'align' =>false )
    ));
    
    // register a video-cta block.
    acf_register_block_type(array(
        'name'              => 'video-cta',
        'title'             => __('Video CTA'),
        'description'       => __('Displays a video with a call to action.'),
        'render_template'   => 'builder/builder-video-cta.php',
        'category'          => 'layout',
        'icon'              => 'video-alt3',
        'keywords'          => array( 'video', 'cta', 'cst' ),
        'supports' => array( 'align' =>false )
    ));
    
    // register a detailed list block.
    acf_register_block_type(array(
        'name'              => 'detailed-list',
        'title'             => __('Detailed List'),
        'description'       => __('An expanded list with an image style option.'),
        'render_template'   => 'builder/builder-list.php',
        'category'          => 'formatting',
        'icon'              => 'excerpt-view',
        'keywords'          => array( 'list', 'descriptive', 'cst' ),
        'supports' => array( 'align' =>false ),
        'mode' => 'auto',
//        'enqueue_script' => get_template_directory_uri() . '/js/detailed-list-block.js',
    ));
    
    // register a tech specs list
    acf_register_block_type(array(
        'name'              => 'tech-specs',
        'title'             => __('Tech Specs'),
        'description'       => __('For technical specifications'),
        'render_template'   => 'builder/builder-specs.php',
        'category'          => 'formatting',
        'icon'              => 'admin-generic',
        'keywords'          => array( 'list', 'tech', 'cst' ),
        'mode'              => 'auto',
        'supports' => array( 'align' =>false )
    ));

    // register a highlight
    acf_register_block_type(array(
        'name'              => 'highlight',
        'title'             => __('Highlight'),
        'description'       => __('For highlighting something with a text and image field'),
        'render_template'   => 'builder/builder-highlight.php',
        'category'          => 'formatting',
        'icon'              => 'visibility',
        'keywords'          => array( 'column', 'product', 'cst' ),
//        'mode'              => 'auto',
        'supports' => array( 'align' =>false )
    ));
    
    // register Grid Content
    acf_register_block_type(array(
        'name'              => 'grid',
        'title'             => __('Grid'),
        'description'       => __('For highlighting something with a text and image field'),
        'render_template'   => 'builder/builder-grid.php',
        'category'          => 'formatting',
        'icon'              => 'screenoptions',
        'keywords'          => array( 'cta', 'duo', 'cst' ),
        'mode'              => 'auto',
        'supports' => array( 'align' =>false )
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'wo_register_acf_block_types');
}