<?php

/**
 * Lock the template for the Copperstone homepage
 */
function cst_front_page_block_template() {
    if ( ! is_admin() || ! isset( $_GET['post'] ) || get_option('page_on_front') !== $_GET['post'] ) {
        // This is not the post/page we want to limit things to.
        return false;
    }

    $post_type_object = get_post_type_object( 'page' );
    $post_type_object->template = array(
        array( 'acf/video-cta', array(
        ) ),
        array ( 'acf/latest-news', array()),
    );
    $post_type_object->template_lock = '';
}
add_action( 'init', 'cst_front_page_block_template' );