//wp.domReady( () => {

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'default',
		label: 'Default',
		isDefault: true,
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'no-underline',
		label: 'No Underline',
	} );

    wp.blocks.unregisterBlockStyle( 'core/pullquote', 'solid-color' );

//} );

function addBlockClassName( props, blockType ) {
    if(blockType.name === 'core/paragraph') {
        return Object.assign( props, { class: 'simple-p' } );
    }
    return props;
}

wp.hooks.addFilter(
    'blocks.getSaveContent.extraProps',
    'gdt-guten-plugin/add-block-class-name',
    addBlockClassName
);
//
//function addListBlockClassName( settings, name ) {
//    if ( name !== 'core/list' ) {
//        return settings;
//    }
// 
//    return lodash.assign( {}, settings, {
//        supports: lodash.assign( {}, settings.supports, {
//            className: true
//        } ),
//    } );
//}
// 
//wp.hooks.addFilter(
//    'blocks.registerBlockType',
//    'my-plugin/class-names/list-block',
//    addListBlockClassName
//);

//function addBackgroundColorStyle( props ) {
//    return lodash.assign( props, { class: 'simple-p' } );
//}
// 
//wp.hooks.addFilter(
//    'blocks.getSaveContent.extraProps',
//    'my-plugin/add-background-color-style',
//    addBackgroundColorStyle
//);