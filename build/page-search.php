<?php
/**
 * Template Name: Search
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php
		while ( have_posts() ) : the_post(); ?>

            <article class="try-searching">
                <div class="entry-content">
                    <?php the_content(); ?>
                    <?php get_search_form(); ?>
                </div>
            </article>
        <?php endwhile; ?>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
