<?php

/**
 * Video CTA Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'highlight-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'highlight';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$img = get_field('image'); // returns an id
$text = get_field('text');

?>
<section id="<?php echo esc_attr($id); ?>" class="builder <?php echo esc_attr($className); ?> padding-left">

    <div class="product-text">
       <?php echo $text; ?> 
    </div>
    <div class="product-image">
       <?php echo wp_get_attachment_image($img, 'full'); ?>
    </div>
    
</section>

