<?php

/**
 * Technical Specifications Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tech-specs-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'builder tech-specs';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$heading = get_field('heading');
$specs = get_field('specifications');

?>
<section id="<?php echo esc_attr($id); ?>" class="builder <?php echo esc_attr($className) . ' ' . $style; ?> padding-left">
    <div class="spec-wrapper">
        <h3><?php echo $heading; ?></h3>
        <div class="specifications">
            <ul class="list-wrapper">
            <?php foreach ($specs as $item) : ?>
                <li>
                   <?php if ($item['specification']) : ?>
                    <div class="specification"><p><?php echo $item['specification']; ?></p></div>
                    <?php endif; ?>
                    <?php if ($item['details']) : ?>
                    <div class="content-wrapper">
                        <?php echo $item['details']; ?>
                    </div>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</section>